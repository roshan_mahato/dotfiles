#!/bin/env sh

#pkill polybar

killall -q polybar
while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done

if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload one -c ~/.config/polybar/config &
    MONITOR=$m polybar --reload two -c ~/.config/polybar/config &
    MONITOR=$m polybar --reload three -c ~/.config/polybar/config &
  done
else
  polybar --reload three &
fi

# polybar two &
# polybar three &
